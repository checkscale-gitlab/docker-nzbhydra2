# docker-nzbhydra2

[![pipeline status](https://gitlab.com/homesrvr/docker-nzbhydra2/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-nzbhydra2/commits/main) 
[![Sabnzbd Release](https://gitlab.com/homesrvr/docker-nzbhydra2/-/jobs/artifacts/main/raw/release.svg?job=PublishBadge)](https://gitlab.com/homesrvr/docker-nzbhydra2/-/jobs/artifacts/main/raw/release.txt?job=PublishBadge)

This is a alpine-based dockerized build of nzbhydra2.

*DO NOT USE IN PRODUCTION. THIS IS A NON-RELEASE VERSION! urgent issues need to be fixed*

## Example usage

docker run example
````
docker run -d \
  -v [/configdir]:/config \
  -v [/completedir]:/complete \
  -v [/incompletedir]:/incomplete \
  -p 5076:5076 \
  --restart=unless-stopped culater/nzbhydra2
````

docker-compose example:
````
---
version: "2.1"
services:
  nzbhydra2:
    image: culater/nzbhydra2
    container_name: nzbhydra2
    hostname: nzbhydra2
    environment:
    - PUID=1001
    - PGID=100
    - TZ=Europe/Berlin
    volumes:
    - /config:/config
    - /downloads:/downloads
    ports:
    - 5076:5076
    restart: always
````

## Authors and acknowledgment
More information about nzbhydra2 can be found here:
[nzbhydra2](https://github.com/theotherp/nzbhydra2 "nzbhydra2 Github Project Homepage") 

## Project status
The docker image auto-updates weekly and should catch up to the latest nzbhydra2 release. 
The release tag will display the nzbhydra2 release the docker image is built upon. 
